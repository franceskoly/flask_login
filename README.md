# App Login con Flask
- Se estará desarrollando una aplicación 

1) Login: Librería Flask-Login

Método Flask-Login: Proporciona administración de sesiones de usuario para Flask. Maneja las tareas de iniciar sesión y cerrar sesión, asi como recordar las sesiones de sus usuarios.

permite:

- Almacenar el Id del usuario activo en __flask_session__, con esto se cierra e inicia sesión fácilmente.
- Restringir las vistas a los usuarios conectados (no conectados) con __login_required__
- Manejar la funcionalidad "recuerdame".
- Ayuda a proteger las sesiones de sus usuariosa través de la cookies. 

Método: current_user: proporciona todos los campos del usuario actual, en nuestro caso tenemos (id, username, email y password), esto se puede emplear en las vistas llamando al metodo asi como tambien como en las plantillas con jinja2.

método logout: cierra la sesión de un usuario, limpia el cookie de recordarme si existe.

2) Conexión a una BD (Flask-SQLALchemy y Postgresql)

- Flask_SQLAlchemy: extensión de Flask que agrega soporte para SQLAlchemy.
- SQLAlchemy: kit de herramientas SQL de Python y asginador relacional de objetos.

- Es una extension de Flask que proporcionar un puente entre Flask y SQALchemy, una biblioteca de asignación relacional de objetos (ORM) para python. Proporciona patrones comunes para conectarse a una base de datos, definir modelos y ejecutar consultas; tambien brinda soporte a las migraciones de BD.