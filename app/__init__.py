from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

# instancias
db = SQLAlchemy()
migrate = Migrate()
bootstrap = Bootstrap()
login_manager = LoginManager()

def create_app(settings_module):
    app = Flask(__name__)
    app.config.from_object(settings_module) # captura el entorno en el que se va a trabajar
    db.init_app(app)
    migrate.init_app(app,db)
    bootstrap.init_app(app)
    login_manager.init_app(app)

    login_manager.login_view = "signin"

    from app.admin import admin
    from app.auth import auth

    app.register_blueprint(admin)
    app.register_blueprint(auth)

    with app.app_context():
        db.create_all()
        
    return app
