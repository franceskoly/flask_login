from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, TextAreaField
from wtforms.validators import DataRequired, Length

class CourseForm(FlaskForm):

    name = StringField(validators=[DataRequired(), Length(max=256)])
    title = StringField(validators=[DataRequired(), Length(max=256)])
    description = TextAreaField(validators=[DataRequired()])
    url = StringField(validators=[DataRequired()])
    module = IntegerField(validators=[DataRequired()])
    chapter = IntegerField(validators=[DataRequired()])
    category = StringField(validators=[DataRequired()])