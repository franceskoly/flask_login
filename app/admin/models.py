from app import db

class Course(db.Model):
    __tablename__ = "login_courses"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String, nullable = False)
    title = db.Column(db.String, nullable = False)
    description = db.Column(db.Text, nullable = False)
    url = db.Column(db.String, unique = True, nullable = False)
    module = db.Column(db.Integer, nullable = False) 
    chapter = db.Column(db.Integer, nullable = False)
    category = db.Column(db.String, nullable = False)
    user_id = db.Column(db.Integer, db.ForeignKey('login_users.id'), nullable = False)

    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    def delete(self):
         db.session.delete(self)
         db.session.commit()

    @staticmethod
    def get_courses(id):
         return Course.query.filter_by(user_id = id).all()
    
    @staticmethod
    def get_by_id(id):
         return Course.query.get(id)
         