from flask import render_template, redirect, url_for, request
from flask_login import login_required, current_user
from .forms import CourseForm
from .models import Course

from app.auth.models import User, generate_password 

from . import admin


@admin.route('/dashboard/user/password/<int:id>', methods = ['GET', 'POST'])
@admin.route('/dashboard/user/password')
@login_required
def update_password(id:None):

    user = User.get_by_id(id)

    if request.method == "POST":
        new_password = request.form['password']
        user.username, user.email, user.password = user.username, user.email, generate_password(new_password)
        user.save()
        #return redirect(url_for('admin.dashboard_user'))
    return render_template('admin/update_password.html', title = "Update Password")


@admin.route('/dashboard/courses/add', methods=['GET', 'POST'])
@login_required
def add_courses():
    form = CourseForm(request.form)
    
    name, title, description = form.name.data, form.title.data, form.description.data
    url, module, chapter, category = form.url.data, form.module.data, form.chapter.data, form.category.data

    if form.validate_on_submit():

        course = Course(name = name, title = title, description = description, url = url, module = module,
                        chapter = chapter, category = category, user_id = current_user.id )
        
        course.save()

        return redirect(url_for('admin.get_courses'))
    return render_template('admin/course_form.html', title="Add Course", form = form)

@admin.route('/dashboard/courses/delete/<int:id>')
@login_required
def delete_course(id=None):

    course = Course.get_by_id(id)
    course.delete()
    return redirect(url_for('admin.get_courses'))

@admin.route('/dashboard/courses/update/<int:id>', methods=['GET', 'POST'])
@login_required
def update_course(id=None):
    course = Course.get_by_id(id)
    form = CourseForm(obj=course)
    update_text = True
    if form.validate_on_submit():
        course.name, course.title, course.description = form.name.data, form.title.data, form.description.data     
        course.url, course.module, course.chapter, course.categoty = form.url.data, form.module.data, form.chapter.data, form.category.data 
        course.save()
        return redirect(url_for('admin.get_courses'))
    return render_template('admin/course_form.html', form = form, title = "Course Update", update = update_text)


@admin.route('/dashboard/courses')
@login_required
def get_courses():

    courses = Course.get_courses(int(current_user.id))
    
    return render_template('admin/courses.html', title = "Cursos", courses = courses)
