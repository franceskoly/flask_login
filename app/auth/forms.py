from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, EmailField, IntegerField, TextAreaField
from wtforms.validators import DataRequired, Email, EqualTo, Length

class LoginForm(FlaskForm):
    username = StringField(validators=[DataRequired(), Length(min=3, max=50)])
    password = PasswordField(validators=[DataRequired()])
    
    
class SignupForm(FlaskForm):
    
    username = StringField(render_kw={"placeholder": "Nombre" }, validators=[DataRequired(message="Ingresa tu nombre"), Length(min=3, max=50)])
    email = EmailField(render_kw={"placeholder": "Correo Electrónico" }, validators=[DataRequired(message="Ingresa un email"), Email(message="Ingresa un email válido")])
    password = PasswordField(render_kw={"placeholder": "Contraseña" }, validators=[DataRequired(message="Ingresa tu contraseña"), EqualTo('confirm')])
   
