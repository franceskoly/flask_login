from flask import render_template, redirect, url_for, request
from .forms import LoginForm, SignupForm

from flask_login import login_user, login_required, current_user, logout_user
from .models import User, generate_password

from . import auth
from app import login_manager

@auth.route('/dashboard/user', methods = ['GET', 'POST'])
@login_required
def dashboard_user():

    user = User.get_by_id(current_user.id)
    form = SignupForm(obj=user)
    m_pass = True
    b_text = True

    if form.validate_on_submit():
        user.username, user.password = form.username.data, form.password.data
        user.save()
        #return redirect(url_for('add_courses'))
    return render_template("auth/signup.html", title = "User", form = form, password = m_pass, button = b_text )


@auth.route('/')
def index():
    return render_template('auth/index.html', title="INDEX")

@auth.route("/signin", methods = ['GET', 'POST'])
def signin():

    if current_user.is_authenticated:
        return redirect(url_for('auth.dashboard_user'))
    
    form = LoginForm(request.form)

    if request.method == 'POST' and form.validate_on_submit :
    
        my_password = form.password.data 
        my_username = form.username.data

        user = User.get_username(my_username)
        
        if user is not None and user.check_password(my_password):

            login_user(user)

            return redirect(url_for('admin.get_courses'))    
    
    return render_template('auth/signin.html', title="Iniciar Sesión", form = form)

@auth.route('/signup', methods=['GET', 'POST'])
def signup():

    if current_user.is_authenticated:
        return redirect(url_for('auth.dashboard_user'))

    form = SignupForm(request.form)
    
    if request.method == 'POST' and form.validate_on_submit:
        n_username = form.username.data
        n_password = form.password.data
        n_email = form.email.data

        user = User(username=n_username, email=n_email, password=generate_password(n_password))
        user.save()
        
        return redirect(url_for('auth.signin'))
        
    return render_template('auth/signup.html', title="Registrarse", form = form)



@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('auth.signin'))


# cargar usuarios
@login_manager.user_loader
def load_user(user_id):
    return User.get_by_id(int(user_id))