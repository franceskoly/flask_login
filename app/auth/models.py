from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import datetime

from app import db

class User (db.Model, UserMixin):
    __tablename__ = "login_users"
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String, nullable = False)
    email = db.Column(db.String, nullable = False)
    password = db.Column(db.String, nullable = False)
    created = db.Column(db.DateTime, default = datetime.datetime.utcnow)
    courses = db.relationship("Course", backref = "user", lazy = True)
    
    
    def check_password(self, n_password):
       # return check_password_hash(self.password, password ) 
        return check_password_hash(self.password, n_password )

    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_username(username):
        return User.query.filter_by(username = username).first()

    @staticmethod
    def get_by_id(id):
        return User.query.get(id)
    
def generate_password(password):
        return generate_password_hash(password)

